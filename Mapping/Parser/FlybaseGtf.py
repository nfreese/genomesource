"""Functions for reading Phytozome GFF3 files."""

import Mapping.FeatureModel as feature
import Utils.General as utils
import sys

"""
This contains functions for parsing FlyBase GTF:

FlyBase GTF reports these features:

$ gunzip -c dmel-all-r6.03.gtf.gz | cut -f3 | sort | uniq 
3UTR
5UTR
CDS
exon
start_codon
stop_codon

Notes:

Final CDS does not include stop_codon.
First CDS includes start_codon.
Many lines have in-line comments indicating 
dicistronic transcripts, read-through stop codons. 
"""

def gff2feats(fname=None):
    """
    Function: read features from FlyBase GTF format file
    Returns : list of CompoundDNASeqFeature objects representing spliced
              mRNA transcripts
    Args    : fname - name of file to read
    """
    lineNumber = 0
    fh = utils.readfile(fname)
    feats={}
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        try:
            (seqname,producer,feat_type,start,end,
             ignore1,strand,ignore2,extra_feat)=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception at line %i:\n%s" % (lineNumber,line))
            raise
        if feat_type in ['exon','CDS']:
            start = int(start)-1
            end = int(end)
            length=end-start
            if strand == '+':
                strand = 1
            elif strand == '-':
                strand = -1
            elif strand == '.':
                strand = None
            try:
                key_vals = parseKeyVals(extra_feat)
            except ValueError:
                sys.stderr.write("Exception at line %i:\n%s" % (lineNumber,line))
                raise
            display_id=key_vals['transcript_id']
            feat = feature.DNASeqFeature(seqname=seqname,
                                         start=start,
                                         length=length,
                                         strand=strand,
                                         key_vals=key_vals,
                                         feat_type=feat_type)
            if not feats.has_key(display_id):
                mRNA = feature.CompoundDNASeqFeature(seqname=seqname,
                                                    display_id=display_id,
                                                    strand=strand,
                                                    key_vals=key_vals,
                                                    feat_type='mRNA')
                feats[display_id]=mRNA
            else:
                mRNA=feats[display_id]
            mRNA.addFeat(feat)
    for mRNA in feats.values():
        try:
            mRNA.useSubFeatCoords()
        except ValueError:
            sys.stderr.write("Error with mRNA: %s\n"%mRNA.getDisplayId())
            raise
    return feats

def parseKeyVals(extra_feat):
    """
    Parse extra feature fields.
    FlyBase GTF extra feature field looks like:
    gene_id "FBgn0037664"; gene_symbol "CG8420"; transcript_id "FBtr0333190"; transcript_symbol "CG8420-RB"

    I'll put all this in the BED-detail field 14 to enable searching, but it would be
    much nicer to use a curator summary like with the Arabidopsis gene models. 

    Note: Some lines have in-line comments:

    3R	FlyBase	CDS	16931402	16931605	3	-	0	gene_id "FBgn0000015"; gene_symbol "Abd-B"; transcript_id "FBtr0083381"; transcript_symbol "Abd-B-RC"; # SO:0000697:gene_with_stop_codon_read_through

    """
    if extra_feat.endswith(';'): # sometimes this happens
        extra_feat=extra_feat[0:-1]
    vals = extra_feat.split('; ')
    key_vals = {}
    for item in vals:
        pair=item.split(' ')
        if not len(pair)>1:
            raise ValueError("GTF line has weird extra feature value: %s."%extra_feat)
        key = pair[0]
        val = pair[1]
        if key == '#':
            sys.stderr.write("Warning: comment character in %s\n"%extra_feat)
            sys.stderr.write("Ignoring it.\n")
            continue
        if not val.startswith('"') or not val.endswith('"'):
            raise ValueError("GTF value not enclosed in quotes: %s."%val)
        val=pair[1][1:-1]
        if key_vals.has_key(key):
            raise ValueError("GTF line has two values for same key in extra feat field: %s."%extra_feat) 
        key_vals[key]=val
    return key_vals
            
        
"""
FlyBase GTF includes UTR, stop_codon, start_codon features, but I think we can safely ignore them.

Example lines for a gene model:

$ gunzip -c dmel-all-r6.03.gtf.gz | grep FBtr0072051 
2R	FlyBase	5UTR	23245347	23245399	2	-	.	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";
2R	FlyBase	5UTR	23244834	23244885	2	-	.	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";

# largest start value for an exon
# starts at 23,245,347
2R	FlyBase	exon	23245347	23245399	2	-	.	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";

# starts at 23,244,817
2R	FlyBase	exon	23244817	23244885	2	-	.	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";

# starts at 23,244,652
2R	FlyBase	exon	23244652	23244759	2	-	.	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";

# smallest start value for an exon
# starts at 23,244,044
2R	FlyBase	exon	23244044	23244601	2	-	.	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";

# start codon
# 23,244,831
2R	FlyBase	start_codon	23244831	23244833	.	-	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";
2R	FlyBase	CDS	23244817	23244833	2	-	0	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";
2R	FlyBase	CDS	23244652	23244759	2	-	1	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";
2R	FlyBase	CDS	23244148	23244601	2	-	1	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";
2R	FlyBase	stop_codon	23244145	23244147	.	-	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";
2R	FlyBase	3UTR	23244044	23244144	2	-	.	gene_id "FBgn0040063"; gene_symbol "yip3"; transcript_id "FBtr0072051"; transcript_symbol "yip3-RB";
"""
