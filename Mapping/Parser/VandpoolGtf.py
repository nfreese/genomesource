"""Functions for reading Phytozome GFF3 files."""

#import Mapping.FeatureModel as feature
import Mapping.FeatureFactory as factory
import Utils.General as utils
import sys

"""
This contains functions for parsing Vandepoele Lab GTF, which reports exon, CDS and protein
domain features, but not transcripts. 
"""

def gff2feats(fname=None):
    """
    Function: read features from Vandepoele lab GTF format file
    Returns : CompoundDNASeqFeature objects representing spliced
              mRNA transcripts
    Args    : fname - name of file to read
    """
    lineNumber = 0
    fh = utils.readfile(fname)
    subfeats = {}
    gene2transcript={}
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        try:
            vals=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception occurred at line %i:\n%s" % (lineNumber,line))
            raise
        if vals[2] in ['exon','CDS']:
            feat = factory.makeFeatFromGffFields(vals=vals)
            key_vals = parseKeyVals(vals[-1])
            display_id=key_vals['transcript_id']
            gene_id=key_vals["gene_id"]
            gene2transcript[display_id]=gene_id
            if not subfeats.has_key(display_id):
                subfeats[display_id]=[]
            subfeats[display_id].append(feat)
    mRNAs = {}
    for display_id in subfeats.keys():
        gene_id=gene2transcript[display_id]
        subfeat_list = subfeats[display_id]
        feat = factory.cfeat_from_feats(subfeat_list,display_id=display_id,
                                        group_feat_type='mRNA',
                                        boundary_feat='exon')
        feat.setKeyVals({"gene_id":gene_id})
        mRNAs[display_id]=feat
    return mRNAs.values()

def parseKeyVals(extra_feat):
    """
    Parse extra feature fields. Vanderpoule Gtf extra feature fields look like:
    transcript_id "AT1G33980_ID58"; gene_id "AT1G33980"; gene_name "AT1G33980";
    """
    if not extra_feat.endswith(';'):
        raise ValueError("Gtf extra feat lacks final ;")
    extra_feat=extra_feat[0:-1]
    vals = extra_feat.split('; ')
    key_vals = {}
    for item in vals:
        pair=item.split(' ')
        if not len(pair)==2:
            raise ValueError("GTF line has weird extra feature value: %s."%extra_feat)
        key = pair[0]
        val = pair[1]
        if not val.startswith('"') or not val.endswith('"'):
            raise ValueError("GTF value not enclosed in quotes: %s."%val)
        val=pair[1][1:-1]
        if key_vals.has_key(key):
            raise ValueError("GTF line has two values for same key in extra feat field: %s."%extra_feat) 
        key_vals[key]=val
    return key_vals
            
        
