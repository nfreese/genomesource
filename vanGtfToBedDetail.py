#!/usr/bin/env python

ex="""Read GTF with AtRTD2 annotations and CDSs, write BED14.

Vandepoule lab annotated Arabidopsis AtRTD2 gene model
annotations with CDS features. AtRTD2 gene model annotations
include some of TAIR10, Araport11, and Marquez gene models
and add some new models generated using Cufflinks and
Stringtie. AtRTD2 gene model names indicate the source. 

See:

  https://ics.hutton.ac.uk/atRTD/
  https://www.ncbi.nlm.nih.gov/pubmed/28402429
  https://www.ncbi.nlm.nih.gov/pubmed/22391557 (Marquez)
  http://www.biorxiv.org/content/early/2017/05/11/136770

Summarizing:
  
 * Gene model ids with _Pn extensions are from Araport, where
   n is a whole number and matches the Araport number.
 * Gene model ids with suffix .n are from TAIR10. The number
   (n) is the same as the TAIR10 number.
 * Gene models with _IDn extensions are from Marquez.
 * Everything else is from Stringtie, Cufflinks assemblies,
   or something else.
 * "Something else" are models with suffixes _CRn, _SRn, _Xn
   I cannot find documentation explaining what these are.
 * Most annotation systems use NAME.n NAME+dot-suffix
   convention to name gene models, with NAME indicating the
   locus. 

This script:

1) Converts AtRTD2 gene models to BED-detail format, using
   CDS annotations from Vaneechoutte, et al (Group leader
   Klaas Vandepoele)
2) Preserves TAIR10 names for gene models. Restores Araport
   names for gene models, when possible. (Sometimes there is
   name collision between TAIR and Araport models that have
   the same name but different exon/intron organization,
   such as AT1G01650.2)
3) Renumbers the remaining gene models incrementing .n 
4) Adds the AtRTD2 gene model identifier to the description
   field in the BED14 format

Note that AtRTD2 gene models are not a superset of TAIR10 or
Araport. In fact, they omit many gene models annotated in
TAIR10 and Araport. It's not clear why. Manual examination
alongside RNA-Seq data clearly shows that some earlier model
versions are more correct.

For example: AT1G28140_c1

Conversely, some genes from Marquez 2012 are now obsolete
in TAIR and Araport.

Annotation is hard!

For a comprehensive listing of all these problems, run this
script with -r|--report flag and capture stderr to a file.
This file can then be loaded into Excel or R.

Ex)

Execute in a directory containing expected default
input files:

$ vanGtfToBedDetail.py -r 2>AtRTD2.err 1>AtRTD2-Van.bed

Count genes with problems:

$ grep -v '#' AtRTD2.err | cut -f4 | sort | uniq | wc -l
    2236


"""
import os,sys,re,optparse
import Mapping.Parser.Bed as Bed
import Mapping.Parser.VandpoolGtf as p
import Mapping.FeatureModel
import Utils.General as utils

tair_default='TAIR10.bed.gz'
araport_default="Araport11.bed.gz"
vand_default="AtRTD2-Vandepoele.gtf.gz"
out_default="AtRTD2.bed"

def readGtfFile(fname=vand_default,report=False):
    """
    Function: Read GTF file, return CompondDNASeqFeature objects
    Returns : list of CompoundDNASeqFeature objects
    Args    : fname - file in Vandepoele GTF format
    """
    models = p.gff2feats(fname)
    if report:
        sys.stderr.write("# Read %i gene models from %s.\n"%(len(models),fname))
    return models

def getGeneInfo(fname):
    """
    Function: Get gene description, field 14 of BED14
    Returns : a dictionary, keys are gene identifiers (e.g., AT1G07350)
    Args    : fname - file in BED14 format

    Uses first description encountered in the file. 
    """
    fh=utils.readFile(fname)
    d={}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        line=line.rstrip()
        vals=line.split('\t')
        descr=vals[-1]
        gene_id=vals[3].split('.')[0]
        if not d.has_key(gene_id):
            d[gene_id]=descr
    return d

def getGeneInfos(araport_file=araport_default,
                tair_file=tair_default,
                report=False):
    """
    Function: Get gene description, field 14 of BED14
    Returns : a dictionary, keys are gene identifiers (e.g., AT1G07350)
    Args    : araport_file - Araport gene model annotation file, BED14
              tair_file - TAIR gene model annotation file, BED14
              report - if True, report to stderr
              
    First read TAIR file, get descriptions for all genes in that file.
    Then, get descriptions for any Araport-specific genes from Araport file.
    Files can be compressed, with (.gz) file extension.
    """
    gene_info=getGeneInfo(tair_file)
    araport_info=getGeneInfo(araport_file)
    if report:
        sys.stderr.write("# Read info for %i genes in %s\n"%(len(gene_info.keys()),tair_file))
        sys.stderr.write("# Read info for %i genes in %s\n"%(len(araport_info.keys()),araport_file))
    N=0
    for gene_id in araport_info.keys():
        if not gene_info.has_key(gene_id):
            gene_info[gene_id]=araport_info[gene_id]
            N=N+1
    if report:
        sys.stderr.write("# Added info for %i Araport genes not in TAIR\n"%N)
    return gene_info

def getModelsFix(models,report=False):
    """
    Function: Create new names for AtRTD2 gene models using .N nomenclature, where
              N is a whole number
    Returns : dictionary, keys are old AtRTD2 transcript ids, values are new ids
    Args    : models - output of readGtfFile
              tair_file - BED14 file with TAIR gene model annotations
              araport_file - BED14 file with Araport gene model annotations 
    """
    d = {}
    if report:
        sys.stderr.write("problemcode\tlargestsuffix\tmodels\tgene_id\ttranscript_id\n")
    for model in models:
        locus=model.getVal("gene_id")
        if not d.has_key(locus):
            d[locus]=[]
        d[locus].append(model)
    for gene_id in d.keys():
        subd=getNewIds(gene_id=gene_id,gene_models=d[gene_id],report=report)
        for key in subd.keys():
            d[key]=subd[key]
    return d

def getNewIds(gene_id=None,gene_models=None,report=False):
    """
    Function: Create new names for AtRTD2 gene models using .N nomenclature, where
              N is a whole number
    Returns : dictionary, keys are old AtRTD2 transcript ids, values are new ids
    Args    : gene_models - gene models for one gene, output of readGtfFile
              gene_id - the gene
    """
    to_return={}
    suffixes=[]
    unassigned=[]
    tairs=filter(lambda x:re.match('AT[\dCM]G\d{5}.\d+',x.getDisplayId()),gene_models)
    for model in tairs:
        gene_id=model.getVal("gene_id")
        old_display_id=model.getDisplayId()
        suffix=int(old_display_id.split('.')[1])
        suffixes.append(suffix)
        to_return[old_display_id]=old_display_id
    araports=filter(lambda x:re.match('AT[\dCM]G\d{5}.P\d+',x.getDisplayId()),gene_models)
    for model in araports:
        old_display_id=model.getDisplayId()
        suffix=int(re.match('AT[\dCM]G\d{5}_P(\d+)',old_display_id).groups()[0])
        if not suffix in suffixes:
            new_display_id="%s.%i"%(gene_id,suffix)
            suffixes.append(suffix)
            to_return[old_display_id]=new_display_id
            suffixes.append(suffix)
        else:
            if report:
                sys.stderr.write("TairAraportModelNameCollision\tNA\tNA\t%s\t%s\n"%(gene_id,old_display_id))
    for model in gene_models:
        if not to_return.has_key(model.getDisplayId()):
            unassigned.append(model)
    unassigned.sort(cmp_models)
    model_num=0
    if len(suffixes)==0:
        if report:
            sys.stderr.write("AtRtdHasNoTairAraportModels\tNA\tNA\t%s\t%s\n"%(gene_id,"NA"))
    else:
        model_num=max(suffixes)
    for model in unassigned:
        model_num=model_num+1
        old_display_id=model.getDisplayId()
        new_display_id="%s.%i"%(gene_id,model_num)
        to_return[old_display_id]=new_display_id
    if report:
        if model_num>len(to_return):
            sys.stderr.write("SuffixExceedsModelCount\t%i\t%i\t%s\t%s\n"%(model_num,
                                                                     len(to_return),
                                                                     gene_id,'NA'))
        if model_num<len(to_return):
            raise ValueError("Largest model number %i is too small for %i models for gene %s"%(model_num,
                                                                                               len(to_return),
                                                                                               gene_id))
        for model in unassigned:
            old_display_id=model.getDisplayId()
            new_display_id=to_return[old_display_id]
            (code)=re.match('AT[\dCM]G\d{5}_([a-zA-Z]+)\d+',old_display_id).groups()
            if code == 'P':
                sys.stderr.write("AraportTairNamecollision\tNA\tNA\t%s\t%s\n"%(gene_id,old_display_id))
            elif code in ['CR','RS','X']:
                sys.stderr.write("UndocumentedSuffix\tNA\tNA\t%s\t%s\n"%(gene_id,old_display_id))
    return to_return

def cmp_models(this,that):
    """
    Function: Compare two gene models
    Returns : True if the first model (this) should have an lower value suffix (e.g., .1) than
              the ther gene model (that) (e.g., .2)
    Args    : this, that - CompoundDNASeqFeature objects, output from readGtfFile
    """
    # note: there are gene models id suffixes not discussed in the documentation:
    # AT3G13570_CR2,AT3G13570_SR1,AT5G24470_X6
    rank={'P':1,'ID':2,'JC':3,'JS':4,'c':5,'s':6,'CR':7,'SR':8,'X':9}
    this_id=this.getDisplayId()
    that_id=that.getDisplayId()
    try:
        (this_producer,this_number)=re.match('AT[\dCM]G\d{5}_([PIDJCRScsX]*)(\d+)',this_id).groups()
    except AttributeError:
        sys.stderr.write("Unrecognized id: %s\n"%this_id)
        raise
    try:
        (that_producer,that_number)=re.match('AT[\dCM]G\d{5}_([PIDJCRScsX]*)(\d+)',that_id).groups()
    except AttributeError:
        sys.stderr.write("Unrecognized id: %s\n"%that_id)
        raise
    try:
        this_producer_rank=rank[this_producer]
    except KeyError:
        sys.stderr.write("Unrecognized id: %s\n"%this_id)
        raise
    try:
        that_producer_rank=rank[that_producer]
    except KeyError:
        sys.stderr.write("Unrecognized id: %s\n"%that_id)
        raise
    if this_producer_rank==that_producer_rank:
        return int(this_number)<int(that_number)
    else:
        return this_producer_rank<that_producer_rank

def addDataToModels(models=None,
                    gene_info=None,
                    models_fix=None,
                    report=False):
    if not models:
        models=readGtfFile()
    if not gene_info:
        getGeneInfo()
    if not models_fix:
        getModelsFix()
    for model in models:
        if model.getVal("old_display_id"):
            old_display_id=model.getVal("old_display_id")
        else:
            old_display_id=model.getDisplayId()
            model.setKeyVal("old_display_id",old_display_id)
        gene_id=model.getVal("gene_id")
        try:
            description=gene_info[gene_id]
        except KeyError:
            if report:
                sys.stderr.write("GeneNotInAraportTair\tNA\tNA\t%s\t%s\n"%(gene_id,old_display_id))
            description="NA"
        new_display_id=models_fix[old_display_id]
        description="%s %s"%(description,old_display_id)
        model.setDisplayId(new_display_id)
        model.setKeyVal("field14",description)
    return models

def writeBedFile(fname=None,models=None,report=False):
    N=0
    if fname:
        fh=open(fname,'w')
    else:
        fh=sys.stdout
    for model in models:
        N=N+1
        bedline = Bed.feat2bed(model,'BED14')
        fh.write(bedline)
    if fh:
        fh.close()
    if report:
        sys.stderr.write("# Wrote %i gene models.\n"%N)


def main(bed_file=None,
         gtf_file=vand_default,
         tair_file=tair_default,
         araport_file=araport_default,
         report=False):
    models = readGtfFile(fname=gtf_file,report=report)
    models_fix=getModelsFix(models,report=report)
    gene_info=getGeneInfos(tair_file=tair_file,
                           araport_file=araport_file,
                           report=report)
    addDataToModels(models=models,
                    gene_info=gene_info,
                    models_fix=models_fix)
    writeBedFile(fname=bed_file,models=models,report=report)

if __name__ == '__main__':
    usage = "%prog "+ex
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gtf_file",
                      help="Vanderpoule GTF file. Default is AtRTD2-Vandepoele.gtf.gz [required]",
                      dest="gtf_file",
                      default=vand_default),
    parser.add_option("-b","--bed_file",help="BED14 file to write. If not given, write to stdout [optional]",
                      dest="bed_file")
    parser.add_option("-t","--tair_file",
                      help="TAIR BED14 gene model file. Default is TAIR10.bed.gz [required]",
                      dest="tair_file",
                      default=tair_default)
    parser.add_option("-a","--araport_file",
                      help="Araport BED14 gene model file. Default is Araport11.bed.gz [required]",
                      default=araport_default,dest="araport_file")
    parser.add_option("-r","--report",help="Print error message table to stderr [optional]",default=False,
                      dest="report",action="store_true")
    (options,args)=parser.parse_args()
    main(gtf_file=options.gtf_file,
         bed_file=options.bed_file,
         tair_file=options.tair_file,
         araport_file=options.araport_file,
         report=options.report)

