"""
Checking on whether TopHat over-reports number of alignmetns per read.
"""

import os,sys,re

def checkBam(fn=None):
    i = 0
    cmd =  'samtools view -h %s'%fn
    fh = run(cmd,verbose=True)
    reg = re.compile(r'NH:i:(\d+)')
    d = {}
    while 1:
        line = fh.readline()
        if i % 10000 == 0:
            sys.stderr.write("read %i lines.\n"%i)
        if not line:
            fh.close()
            break
        i = i + 1
        if line.startswith('@'):
            continue
        n = int(reg.findall(line)[0])
        name = line.split('\t')[0]
        if not d.has_key(name):
            d[name]=[n,1]
        else:
            d[name][1]=d[name][1]+1
    not_okays = 0
    for name in d.keys():
        if d[name][0]!=d[name][1]:
            not_okays=not_okays+1
    sys.stderr.write("NH tags disagree on %i reads.\n"%not_okays)
    return d

def run(cmd,verbose=False):
    cwd = os.getcwd()
    if verbose:
        sys.stderr.write("running: %s in %s\n"%(cmd,cwd))
    fh = os.popen(cmd)
    return fh
