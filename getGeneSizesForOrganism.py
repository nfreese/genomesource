#!/usr/bin/env python
ex=\
"""
A script that consumes BED files and gene information files
from NCBI and outputs gene length files.

Requires:

BED file from UCSC Genome Bioinformatics Table Browser or IGB
Quickload (for plant genomes)

and

ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2accession.gz

and

ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz

"""

import Utils.General as utils
import os,sys,optparse

file2tax_id={"M_musculus_Dec_2011_refGene.bed.gz":"10090",
             "C_elegans_Feb_2013.refGene.bed.gz":"6239",
             "TAIR10_mRNA.bed.gz":"3702",
             "D_melanogaster_Jul_2014.refGene.bed.gz":"7227",
             "H_sapiens_Dec_2013_refGene.bed.gz":"9606"}

def doAllInThisFolder(gene_info=None,
                      gene2accession=None,
                      beds=None,
                      agi2gene_id=None):
    """
    Function: Read BED files and emit gene length data.
    Returns : 
    Args    : 

    Write to sys.stdout.
    """
    valss=[]
    if not gene_info:
        gene_info=readGeneInfo()
    if not beds:
        beds=readBedFiles()
    if not gene2accession:
        gene2accession=readGene2Accession()
    if not agi2gene_id:
        agi2gene_id=AgiToEntrez(gene_info=gene_info)
    gene_lengths = {}
    for tax_id in gene_info.keys():
        if tax_id == '3702':
            gene2bed = {}
            for gene_model_id in beds['3702'].keys():
                gene_id=gene_model_id.split('.')[0]
                if not gene2bed.has_key(gene_model_id):
                    gene2bed[gene_id]={}
                gene2bed[gene_id]={gene_model_id:beds['3702'][gene_model_id]}
            for agi in gene2bed.keys():
                if not agi2gene_id.has_key(agi):
                    continue
                alignments=[]
                gene_id=agi2gene_id[agi].keys()[0]
                alignments=gene2bed[agi].values()[0]
                starts=map(lambda x:int(x[1]),alignments)
                ends=map(lambda x:int(x[2]),alignments)
                start = min(starts)
                end = max(ends)
                description=gene_info[tax_id][gene_id][8]
                symbol=gene_info[tax_id][gene_id][2]
                seqname=alignments[0][0]
                vals=[tax_id,gene_id,description,symbol,seqname,str(start),str(end)]
                valss.append(vals)
            continue
        for gene_id in gene_info[tax_id].keys():
            if not gene2accession[tax_id].has_key(gene_id):
                continue 
            accessions=gene2accession[tax_id][gene_id].keys()
            alignments=[]
            for accession in accessions:
                if beds[tax_id].has_key(accession):
                    alignments.append(beds[tax_id][accession])
            if len(alignments)>0:
                seqname=alignments[0][0][0]
                strand=alignments[0][0][5]
                test1=filter(lambda x:not x[0][0] == seqname,alignments)
                test2=filter(lambda x:not x[0][5] == strand,alignments)
                if len(test1)>0 or len(test2)>0:
                    continue # skip this gene
                else:
                    starts=map(lambda x:int(x[0][1]),alignments)
                    ends=map(lambda x:int(x[0][2]),alignments)
                    start = min(starts)
                    end = max(ends)
                    description=gene_info[tax_id][gene_id][8]
                    symbol=gene_info[tax_id][gene_id][2]
                    vals=[tax_id,gene_id,description,symbol,seqname,str(start),str(end)]
                    valss.append(vals)
    return valss

def writeResults(valss,filename=None):
    heads=['taxonomy.id','gene.id','description','gene.symbol','chromosome','start','end']
    if filename:
        fh=open(filename,'w')
    else:
        fh=sys.stdout
    fh.write('\t'.join(heads)+'\n')
    for vals in valss:
        line='\t'.join(vals)+'\n'
        fh.write(line)
    if filename:
        fh.close()

def readBedFiles():
    """
    Function: read BED files into memory; eliminate transcripts that
              map to multiple locations
    Returns : double dictionary; keys are taxonomy ids, values are
              dictionaries mapping transcript ids onto alignments
              (BED format)
    Args    : 

    Reads BED files indicated in dictionary file2tax_id (defined at
    top of this file)
    """ 
    bed_files = file2tax_id.keys()
    d = {}
    for bed_file in bed_files:
        fh = utils.readfile(bed_file)
        tax_id=file2tax_id[bed_file]
        d[tax_id]={}
        while 1:
            line = fh.readline()
            if not line:
                break
            toks=line.rstrip().split('\t')
            seqname=toks[0]
            if seqname.startswith('chrUn') or seqname.endswith('_alt'):
                continue
            transcript_name=toks[3]
            if d[tax_id].has_key(transcript_name):
                d[tax_id][transcript_name].append(toks)
            else:
                d[tax_id][transcript_name]=[toks]
        for transcript_id in d[tax_id].keys():
            if len(d[tax_id][transcript_id])>1:
                del(d[tax_id][transcript_id]) # no multi-mappers allowed
    return d

def main(bed_file=None,
         out_file=None,
         gene2accession='gene2accession.gz'):
    """
    Function: add gene name and gene description fields (13, 14)
              to a BED 12 file
    Returns : 
    Args    : bed_file - name of BED file to convert
              out_file - name of file to write

    If bed_file is None, read from sys.stdin.
    If out_file is None, write to sys.stdout.
    """ 
    bed = readBed(fn=bed_file)
    gene_info=readGeneInfo()
    acc2gene_id = readGene2Accession(bed=bed,fn=gene2accession)
    mergeBed(bed=bed,gene_info=gene_info,outfile=out_file,acc2gene=acc2gene_id)


def readBed(fn=None):
    """
    Function: read a BED12 format file
    Returns : dictionary, keys are name (field 4) from BED
              values are lists of lists
              one mRNA may have multiple mappings and we store each one
    Args    : fn - name of BED format file
    """ 
    if not fn:
        fh = sys.stdin
    else:
        fh = utils.readfile(fn)
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            if fn:
                fh.close()
            break
        toks = line.rstrip().split('\t')
        name = toks[3]
        if d.has_key(name):
            d[name].append(toks)
        else:
            d[name]=[toks]
    return d

def readGene2Accession(fn='gene2accession.gz',
                       gene_info=None):
    """
    Function: get mapping of genes to accessions
    Returns : triple dictionary, keys are taxonomy ids (from gene_info)
              values are dictionaries, keys are gene_ids and values
              are dictionaries mapping accessions onto their data
              from the file
    Args    : fn - name of gene2accession file from NCBI
 
    Tip: To make this go faster, can pre-filter by taxonomy id, the first
         field in the gene2accession file.
    """
    if not gene_info:
        raise ValueError("Gene info dictionary required.")
    d = {}
    for tax_id in file2tax_id.values():
        d[tax_id]={}
    fh = utils.readfile(fn)
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        toks = line.rstrip().split('\t')
        tax_id = toks[0]
        if not d.has_key(tax_id):
            continue
        gene_id = toks[1]
        if not gene_info[tax_id].has_key(gene_id):
            continue
        accession = toks[3].split('.')[0]
        if accession=='-':
            continue
        if not d[tax_id].has_key(gene_id):
            d[tax_id][gene_id]={accession:toks}
        else:
            d[tax_id][gene_id][accession]=toks
    return d

def getEntrezToAgiArabidopsis(gene_info=None):
    d = {}
    for gene_id in gene_info['3702'].keys():
        agi_code=gene_info['3702'][gene_id][2]
        d[gene_id]={agi_code:gene_info['3702'][gene_id]}
    return d

def AgiToEntrez(gene_info=None):
    d = {}
    for gene_id in gene_info['3702'].keys():
        agi_code=gene_info['3702'][gene_id][3]
        d[agi_code]={gene_id:gene_info['3702'][gene_id]}
    return d

def readGeneInfo(fn='gene_info.gz'):
    fh = utils.readfile(fn)
    d = {}
    tax_ids = file2tax_id.values()
    for tax_id in tax_ids:
        d[tax_id]={}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        toks = line.rstrip().split('\t')
        tax_id = toks[0]
        if tax_id in tax_ids:
            gene_id = toks[1]
            if d[tax_id].has_key(gene_id):
                raise ValueError("Gene id %s present twice in %s"%(gene_id,fn))
            d[tax_id][gene_id]=toks
    for tax_id in d.keys():
        if len(d[tax_id])==0:
            raise ValueError("No data for tax_id: %s"%tax_id)
    return d

def mergeBed(bed=None,gene_info=None,outfile=None,
             acc2gene=None):
    if not outfile:
        outfh = sys.stdout
    else:
        outfh = open(outfile,'w')
    for name in bed.keys():
        tokss = bed[name]
        for toks in tokss:
            if acc2gene.has_key(name):
                gene_id = acc2gene[name]
                symbol=gene_info[gene_id][0]
                description=gene_info[gene_id][1]
            else:
                sys.stderr.write("No gene id for %s.\n"%name)
                symbol='NA'
                description='NA'
            newtoks = list(toks)
            newtoks.append(symbol)
            newtoks.append(description)
            line = '\t'.join(newtoks)+'\n'
            outfh.write(line)
    if outfile:
        outfh.close()

if __name__ == '__main__':
    usage = "%prog [options]\n"+ex
    parser = optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(bed_file=bed_file,
         out_file=out_file,
         gene2accession=options.gene2accession)
