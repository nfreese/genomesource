"""A script that converts TAIR GFF for T-DNA insertions into bed, with color coding similar to what is used by Salk T-DNA Express."""

"""This DOESNT WORK YET. Ann is working on it."""

import gzip,sys,re
import Mapping.Parser.Bed as b
import Mapping.Feature as feature

"""
Some general info:

minerva:genomes.pub admini$ zcat TAIR9_GFF3_tdnas.gff.gz | cut -f1 | sort | uniqChr1
Chr2
Chr3
Chr4
Chr5
ChrC
ChrM

# the file has 371,040 lines in it

minerva:genomes.pub admini$ zcat TAIR9_GFF3_tdnas.gff.gz | cut -f2 | sort | uniqTAIR9
minerva:genomes.pub admini$ zcat TAIR9_GFF3_tdnas.gff.gz | cut -f3 | sort | uniq 
transposable_element_insertion_site
"""
# pink SALK T-DNA
salk_tdna="239,51,153"
# green SALK T-DNA Homozygous
salk_tdna_hm = "0,153,0"
# orange SAIL FST
sail_fst="192,120,41"
# dark yellow GABI-KAT FST
gabi_kat_fst="153,153,53"
# greenish Wisc FST
wisc_fst="80,170,32"

def readGFF(fn='TAIR9_GFF3_tdnas.gff.gz'):
    if fn.endswith('.gz'):
        fh = gzip.GzipFile(fn)
    else:
        fh = open(fn,'r')
    feats = []
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks = line.rstrip().split('\t')

        f = makeFeat(toks)
        feats.append(f)
    return feats

def makeFeat(toks):
    seqname = 'chr'+toks[0][-1]
    start = int(toks[3])-1
    length = 0
    strand = 1
    producer='TAIR9'
    display_id=toks[-1].split('=')[-1]
    f = feature.CompoundDNASeqFeature(start=start,
                                      length=0,
                                      strand=strand,
                                      seqname=seqname,
                                      producer=producer,
                                      display_id=display_id,
                                      feat_type='mRNA')# hack
    rgb="0,0,0"
    if display_id.startswith('SALK'):
        if display_id.endswith('c')
    return f
    
def writeBed(fn='inserts.bed',feats=None):
    fh = open(fn,'w')
    for feat in feats:
        line = b.feat2bed(feat)
        fh.write(line+'\n')
    fh.close()

