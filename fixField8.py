#!/usr/bin/env python

"""
Replace field 8 (rgb) in a BED file with 0.

Usage:

 fixField8.py file.bed > newfile.bed
 cat file.bed | fixField8.py > newfile.bed

Reads from stdin or from file (or files) given as arguments.
Outputs on stdout.
"""

import sys,fileinput

def main():
    for line in fileinput.input():
        toks=line.rstrip().split('\t')
        toks[8]='0'
        line='\t'.join(toks)+'\n'
        sys.stdout.write(line)

if __name__ == '__main__':
    main()

