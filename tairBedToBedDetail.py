#!/usr/bin/env python

"""
Use text from TAIR10_functional_descriptions.txt.gz and gene_aliases.20120207.txt.gz
to add gene name and description columns to a BED format file.

Requires:

  TAIR10_functional_descriptions.txt.gz
  gene_aliases.20120207.txt.gz

Both must reside in the current working directory.

Invoke as:

  $ cat file.bed | tairBedToBedDetail.py > newfile.bed

Columns in TAIR10_functional_descriptions.txt.gz:

Model_name
Type
Short_description
Curator_summary
Computational_description

Columns in gene_aliases.20120207.txt.gz:

locus_name
symbol
full_name

WARNING: this original version of
TAIR10_functional_descriptions.txt.gz from TAIR contained non-ASCII
characters in 70 lines.

I removed the non-ASCII characters using cleanNonAsciiChars.py.

Also, see:

http://stackoverflow.com/questions/2743070/removing-non-ascii-characters-from-a-string-using-python-django
"""
import fileinput,sys,gzip

def readFuncAnnot(fn='TAIR10_functional_descriptions.txt.gz'):
    fh = gzip.GzipFile(fn)
    heads=fh.readline().rstrip().split('\t')
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks=line.split('\t')
        model_name = toks[0] # ex) AT1G01010.1
        if d.has_key(model_name):
            raise ValueError("Duplicate for %s on line %s"%(model_name,line))
        else:
            d[model_name]=toks
    return d

def readAliases(fn='gene_aliases.20120207.txt'):
    fh = open(fn,'r')
    heads = fh.readline().rstrip().split('\t')
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks=line.split('\t')
        locus_name=toks[0]
        i = 1
        while i < 3:
            if toks[i] in ['','\n']:
                toks[i]=None
            i=i+1
        if d.has_key(locus_name):
            d[locus_name].append(toks)
        else:
            d[locus_name]=[toks]
    return d

def getDescr(model_name,func_d,alias_d):
    vals=func_d[model_name] # every model should have a line in func_d
    # if there's a Curator_summary, use that
    if vals[3]!='':
        return vals[3]
    # if there isn't a Curator_summary but there is a Short_description, use that
    if vals[2]!='':
        return vals[2]
    # if there isn't either of these, try getting the long-form gene name from
    # the alias_d
    locus_id=model_name.split('.')[0]
    if alias_d.has_key(locus_id):
        lstss=alias_d[locus_id]
        descrs=map(lambda x:x[2],lsts)
        descrs=filter(lambda x:x,descrs) # get rid of None values
        if len(descrs)>0:
            to_use=None
            for descr in descrs:
                if 'arabdopsis' in descr: # avoid stupid typo
                    sys.stderr.write("Warning: description type for %s: %s\n"%(locus_id,descr))
                    continue
                if to_use==None or len(to_use)<descr:
                    to_use=descr
            if to_use:
                return to_use
    else:
        return None

def main():
    func_d=readFuncAnnot()
    alias_d=readAliases()
    for line in fileinput.input():
        toks=line.rstrip().split('\t')
        model_name=toks[3]
        locus_name=model_name.split('.')[0]
        # first get the symbol(s)
        if not alias_d.has_key(locus_name):
            symbol=locus_name
        else:
            lsts=alias_d[locus_name]
            # Note: if any symbol is None, we should get an error.
            symbol='|'.join(map(lambda x:x[1],lsts)) 
        descr=getDescr(model_name,func_d,alias_d)
        if not descr:
            descr = symbol
        toks.append(symbol)
        toks.append(descr)
        newline='\t'.join(toks)+'\n'
        sys.stdout.write(newline)

if __name__ == '__main__':
    main()
